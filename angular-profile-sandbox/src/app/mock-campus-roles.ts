import { CampusRole } from './campus-role';

export const CAMPUS_ROLES: CampusRole[] = [
  { name: 'Faculty/Staff',
    description: "Employment at the university",
    activeness: true
  },
  { name: 'Researcher',
    description: "The role of sifting and winnowing",
    activeness: true
  },
  { name: 'Student',
    description: "Supplicant in pursuit of a degree",
    activeness: false
  },
  { name: 'Campus Sustainability Committee Chair',
    description: "Leadership position on the committee",
    activeness: true
  },
];
