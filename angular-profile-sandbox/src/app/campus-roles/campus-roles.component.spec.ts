import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusRolesComponent } from './campus-roles.component';

describe('CampusRolesComponent', () => {
  let component: CampusRolesComponent;
  let fixture: ComponentFixture<CampusRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampusRolesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
