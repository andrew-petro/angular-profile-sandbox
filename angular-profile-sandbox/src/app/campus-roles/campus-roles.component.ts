import { Component, OnInit } from '@angular/core';
import { CampusRole } from '../campus-role';
import { CAMPUS_ROLES } from '../mock-campus-roles';

@Component({
  selector: 'app-campus-roles',
  templateUrl: './campus-roles.component.html',
  styleUrls: ['./campus-roles.component.css']
})
export class CampusRolesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  campusRoles = CAMPUS_ROLES;

}
