export interface Application {
  id: number;
  name: string;
  hasAccessTo: string;
  hasAccessToDescription: string;
  accessGrantedTimestamp: string;
  accessGivenBy: string;
  accessExpiresTimestamp: string;
}
