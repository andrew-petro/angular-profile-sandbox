import { Application } from './application';

export const APPLICATIONS: Application[] = [
  { id: 11, name: 'Microsoft Office 365',
    hasAccessTo: "Basic directory information",
    hasAccessToDescription: "Basic directory information about you, such as your preferred name and your email address.",
    accessGrantedTimestamp: "Dec 22, 2019 at 8:05 AM",
    accessGivenBy: "James Smith",
    accessExpiresTimestamp: "Dec 22, 2025 at 12:01 AM",
  },
  { id: 12, name: 'JIRA',
    hasAccessTo: "Basic directory information",
    hasAccessToDescription: "Basic directory information about you, such as your preferred name and your email address.",
    accessGrantedTimestamp: "Nov 12, 2018 at 3:05 PM",
    accessGivenBy: "Ada Lovelace",
    accessExpiresTimestamp: "Nov 12, 2024 at 12:01 AM",
  },
  { id: 13, name: 'Student Information System',
    hasAccessTo: "Basic directory information; Academic records",
    hasAccessToDescription: "Basic directory information about you, such as your preferred name and your email address. Academic records, such as your past, current, and future course schedule and grades.",
    accessGrantedTimestamp: "Jan 15, 2017 at 1:08 PM",
    accessGivenBy: "Charles Babbage",
    accessExpiresTimestamp: "Jan 15, 2023 at 12:01 AM",
  },
  { id: 14, name: 'Trello',
    hasAccessTo: "Basic directory information",
    hasAccessToDescription: "Basic directory information about you, such as your preferred name and your email address.",
    accessGrantedTimestamp: "April 17, 2019 at 3:33 PM",
    accessGivenBy: "Jan Smith",
    accessExpiresTimestamp: "April 17, 2023 at 12:00 AM",
  }
];

