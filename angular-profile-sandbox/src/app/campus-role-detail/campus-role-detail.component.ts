import { Component, OnInit, Input } from '@angular/core';
import { CampusRole } from '../campus-role';

@Component({
  selector: 'app-campus-role-detail',
  templateUrl: './campus-role-detail.component.html',
  styleUrls: ['./campus-role-detail.component.css']
})
export class CampusRoleDetailComponent implements OnInit {

  @Input() campusRole!: CampusRole;

  constructor() { }

  ngOnInit(): void {
  }

}
