import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusRoleDetailComponent } from './campus-role-detail.component';

describe('CampusRoleDetailComponent', () => {
  let component: CampusRoleDetailComponent;
  let fixture: ComponentFixture<CampusRoleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampusRoleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusRoleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
