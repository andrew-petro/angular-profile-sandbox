import { Component, OnInit } from '@angular/core';
import { Application } from '../application';
import { APPLICATIONS } from '../mock-applications';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  selectedApplication!: Application;
  onSelect(application: Application): void {
    this.selectedApplication = application;
  }

  applications = APPLICATIONS;

}

