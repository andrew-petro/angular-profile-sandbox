export interface CampusRole {
  name: string;
  description: string;
  activeness: boolean;
}
