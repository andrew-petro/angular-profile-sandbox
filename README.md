# angular-profile-sandbox

Experimenting with Angular using user profile designs as an exercise.

## Summary screenshot

![Screenshot showing data and personalization page](./docs/assets/screenshot-data-and-personalization.png)

## Journal of this experiment

I've  [journaled step-by-step](./docs/README.md) building this proof of concept.

Also see the git history, where commits represent building the app step by step. Most commits will compile and render with `ng server --open`, so you can time travel to see the app in most steps of its contruction.

## Kicking the tires

Pre-requisites:

+ [install Angular CLI](https://angular.io/guide/setup-local),
+ have Git.

Clone this repo.

`git clone https://git.doit.wisc.edu/andrew-petro/angular-profile-sandbox.git`

navigate into the repo *and into the project directory inside the repo*

```shell
$ cd ./angular-profile-sandbox
$ cd ./angular-profile-sandbox
```

Build and serve the application using Angular CLI

```shell
$ ng serve --open

chunk {main} main.js, main.js.map (main) 61.6 kB [initial] [rendered]
chunk {polyfills} polyfills.js, polyfills.js.map (polyfills) 141 kB [initial] [rendered]
chunk {runtime} runtime.js, runtime.js.map (runtime) 6.15 kB [entry] [rendered]
chunk {styles} styles.js, styles.js.map (styles) 340 kB [initial] [rendered]
chunk {vendor} vendor.js, vendor.js.map (vendor) 3.86 MB [initial] [rendered]
Date: 2020-11-12T15:29:26.011Z - Hash: 0410e02d39a180e267de - Time: 9379ms
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
: Compiled successfully.
```

Your browser will pop open to <http://localhost:4200/> and you see the app with mock data.

## Connecting to a real back end

So far this demo app just uses local mock data[^data-not-markup].
It does not connect to any back-end.

How might a front-end app like this connect to real back-end services so that it'd show real profile data and even write back people's requested changes?

One future architectural possibility for an app like this would be to integrate with an API gateway.

```mermaid
graph BT
    profile[this end-user-facing profile UX in browser] -->|Reads and writes JSON| gateway(API gateway)
    gateway --> sf-api(Salesforce API maybe via Apex classes)
    sf-api --> sf-data[(Salesforce data)]
    sf-admin[staff-facing CRM and dashboard and reporting UIs in Salesforce] --> sf-data
```

and then on the other side of that API gateway could be e.g. Salesforce as the place to store, manage, administer, report on profile data. Administrators and staff working with users' profile data might do that in Salesforce using the CRM functionality. End-users accessing their own profile on a self-service basis would use a front-end app like this that talks to the API gateway that talks to Salesforce.

[^data-not-markup]: While it's mock data rather than real data coming from a real back end, it *is* really *data* in some places, not just hard coded markup. That is, the concept being explored here isn't just that Angular and Angular Material can make a *static page* look Material -- this is exploring viability of a dynamic, interactive *web application*.
