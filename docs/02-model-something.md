# Modeling applications with access to my data

Okay, I still have no idea what I'm doing.
But I figure we need to model some objects.
One relatively simple object in the mockups is
"Applications with access to your information".

So let's start there.

`ng generate component applications`

In `applications.component.ts`, expose an application property.

```typescript
  application = "Trello"
```

In the template `applications.component.html`, display that property.

```html
{{application}}
```

In the main template `app.component.html`, plop in the applications component.

And Trello shows up on the page.

We also need individual Applications to be a thing.  So,

Add an interface in `application.ts`. Start simple with a name and an id.

```typescript
export interface Application {
  id: number;
  name: string;
}
```

Instantiate an example application in applications

```typescript
  application: Application = {
    id: 1,
    name: "Trello"
  }
```

Tweak the applications template to render this new data model

```html
<h2>{{application.name}} details</h2>
<div><span>id: </span>{{application.id}}</div>
<div><span>name: </span>{{application.name}}</div>
```

This too renders.



Okay, we probably don't actually need applications to be live editable in the
profile. They're probably more something that there are few of and adding them
is a big deal. But, for demo purposes at least, let's make these editable.

The applications template will need to have a form

```html
<div>
  <label>name:
    <input [(ngModel)]="application.name" placeholder="name"/>
  </label>
</div>
```

and add forms support in app.module.ts

```typescript
import { FormsModule } from '@angular/forms';
```

and use the newly imported support

```typescript
@NgModule({
  ...
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  ...
})
```

Now we can live edit the application name.

Let's add some mock data along the lines of that in the mockups.

```typescript
import { Application } from './application';

export const APPLICATIONS: Application[] = [
  { id: 11, name: 'Trello' },
  { id: 12, name: 'Office365' },
  { id: 13, name: 'JIRA' },
  { id: 14, name: 'Student Information System' }
];
```

This is the kind of thing where the data would be coming from the back-end
if we were doing this for real, but for demo purposes,
mocking out what we would have gotten from a back end.

Grab that constant in Applications

```
import { APPLICATIONS } from '../mock-applications';
...
applications = APPLICATIONS;
```

and display the list of applications

```html
<h2>Applications</h2>
<ul class="applications">
  <li *ngFor="let application of applications">
    <span class="badge">{{application.id}}</span> {{application.name}}
  </li>
</ul>
```

Make applications selectable with a click event

call onSelect on click

```html
<li *ngFor="let application of applications" (click)="onSelect(application)">
```

and handle the onClick event

```typescript
  selectedApplication: Application;
  onSelect(application: Application): void {
    this.selectedApplication = application;
  }
```

and predicate showing details on their being a selection

```html
<div *ngIf="selectedApplication">
```

and style the selected application

```html
[class.selected]="application === selectedApplication"
```

Okay. Now it lists applications and you can select an application
and it reflects which is selected in a couple of ways.

Which starts to look a bit like the application listing and details selection
in the mockup.


