# Factoring out application details from applications

The prior steps made a bit of a mess in mixing together showing the list of
applications and the details about a specific application. Really those are two
different things. So here we make application details a more fully-fledged
respectable implementation.

`ng generate component application-detail`

Move the details markup from the applications template
into the new application-detail template.

Target `application` rather than `selectedApplication`
since the details component doesn't need to have an opinion about selection,
it just renders the application it's told to render.

```html
<div *ngIf="application">
  <h2>{{application.name}} details</h2>

  <div>
    <label>name:
      <input [(ngModel)]="application.name" placeholder="name"/>
    </label>
  </div>
</div>
```

and adjust the applications template to use application-detail.

```html
<app-application-detail [application]="selectedApplication"></app-application-detail>
```
