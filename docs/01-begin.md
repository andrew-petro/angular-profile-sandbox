# 01 - begin

Working from the [guide on setting up a local development environment](https://angular.io/guide/setup-local).

Have the right dependencies.

+ Installed latest Node, using `nvm` (Node Version Manager).
+ Installed Angular CLI, by `npm install -g @angular/cli`

Created this git repo on git.doit, to have a place to capture these notes.
Cloned it down to my laptop. Navigated into it in the terminal
(and opened it in Visual Studio Code so I could write these notes.)

Used the Angular CLI to make a new application

`ng new angular-profile-sandbox --strict`

Accepted the defaults for how to do routing and CSS.

This created the angular-profile-sandbox directory with the generated application in it.

`cd angular-profile-sandbox`

Kicked tires.

`ng serve --open`

The default welcome page rendered. Okay.

I have no idea what I'm doing. But I'm going to want to use Material,
and right on the welcome page it suggests `ng add @angular/material`, so

`ng add @angular/material`

chose the custom theme colors since I'll want to give it Madison colors,
and otherwise accepted defaults as to typography and animations.

Okay. I still have no idea what I'm doing.

But let's follow along with the "[Tour of Heroes][]" example application,
except building this application.

Adjust the title and add a description in `app.component.ts`

```typescript
export class AppComponent {
  title = 'Angular Profile Sandbox';
  description = 'An exploration of using Angular to implement Material user interfaces.'
}
```

and the template in `app.component.html`

```html
<h1>{{title}}</h1>
<p>{{description}}</p>
```

just to see something change. Those margins are ugly, tweak them in `styles.css`

```css
html, body { height: 100%; }
body { margin: 2em; font-family: Roboto, "Helvetica Neue", sans-serif; }
```

[Tour of Heroes]: https://angular.io/tutorial
