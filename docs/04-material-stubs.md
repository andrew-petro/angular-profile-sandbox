# Starting to show some Material

Okay, let's get this looking and structured more like the mockup.

First, let's demonstrate using a Material component at all.

` ng generate @angular/material:navigation main-page`

and then harvest out of the generated main-page to
build the toolbar and navigation in the root template.

```html
<mat-toolbar color="primary">
  <button
    type="button"
    aria-label="Toggle sidenav"
    mat-icon-button
    (click)="drawer.toggle()"
    >
    <mat-icon aria-label="Side nav toggle icon">menu</mat-icon>
  </button>
  <span>{{title}}</span>
  <span class="toolbar-spacer"></span>
  <mat-icon aria-hidden="false" aria-label="Help">help</mat-icon>
</mat-toolbar>

<mat-sidenav-container class="sidenav-container">
  <mat-sidenav #drawer class="sidenav" fixedInViewport
      role="navigation"
      mode="side"
      fixedTopGap="64"
      opened>

    <mat-nav-list>
      <a mat-list-item href="#">Personal profile</a>
      <a mat-list-item class="mat-list-item-selected" href="#">Data & personalization</a>
      <a mat-list-item href="#">Security</a>
      <a mat-list-item href="#">People & sharing</a>
      <a mat-list-item href="#">Notification settings</a>
      <a mat-list-item href="#">Help</a>
      <a mat-list-item href="#">Feedback</a>
    </mat-nav-list>
  </mat-sidenav>


  <mat-sidenav-content>
    <h1>{{title}}</h1>
    <p>{{description}}</p>
    <div>
      <app-applications></app-applications>
    </div>

  </mat-sidenav-content>
</mat-sidenav-container>
```

The navigation doesn't do anything for now, and it's hard-coded to look like
the "Data & personalization" tab is selected, since that's where the
applications we were messing with earlier would live.

I added the help icon and the styles to situate the side nav below the app
bar like in the mockups. I added a menu button so it's at least possible for
users in narrow browsers (mobile!) to hide the side nav. The responsive design
treatment will eventually need a bit more love -- at mobile widths the side nav
could maybe modal. Anyway the mockups aren't responsive so let's call this
good enough for now.

The margins, rounded corners, and the vertical line in the mockups are all nice.
Some CSS love could achieve all of that. Let's resist diving into those tweaks
for now.

Using a custom theme, and set some colors in `custom-theme.scss` to get to
red-ish. To really do this right we'd need a Madison-specific Material theme
(colors and font). That too is totally doable but a bit tedious to definte all
the colors in the palatte and test it for contrast and so forth. Let's call
red-ish good enough to give the idea for now.

Since it looks like we're on the "Data & personalization page",
let's add the flavor text.

And make that "Applications with access to your information" look more like a
card.

So, import Material cards,

```typescript
import {MatCardModule} from '@angular/material/card';
...
```

Use the card

```html
<mat-card>
<mat-card-title>Applications with access to your information</mat-card-title>
<mat-card-content>
...
```

a little CSS gets closer to the flair in the mockups

```css
.mat-card {
  border-top: 12px;
  border-color: #04798a;
  border-top-style: solid;
  margin: 10px;
}
```

That color should probably be the accent color from the theme. I'll probably need a more specific selector, once there's a use for Material cards that shouldn't have the accent color bar across the top, but for now this'll do.

Nudge the padding a bit to give the content some breathing room.

```css
.mat-sidenav-content {
  padding-left: 10px;
}
```

In the mockup, the listing of applications looks a lot like a Material Accordian, where the items are expansion panels. Let's sketch that.

import the dependency, and then

```html
<mat-accordian multi>
  <mat-expansion-panel *ngFor="let application of applications">
    <mat-expansion-panel-header>
      <mat-panel-title>{{application.name}}</mat-panel-title>
    </mat-expansion-panel-header>
    <app-application-detail [application]="application"></app-application-detail>
  </mat-expansion-panel>
</mat-accordian>
```

Let's go ahead and model some of the details.
Using strings for demo purposes. For real we'd want more strongly typed data.

```typescript
export interface Application {
  id: number;
  name: string;
  hasAccessTo: string;
  hasAccessToDescription: string;
  accessGrantedTimestamp: string;
  accessGivenBy: string;
  accessExpiresTimestamp: string;
}
```

and update the mock data to give some flavor.

Change the `application-detail` template to stop being the un-needed edit form and instead be the stylized details about the access granted to the specific application.

Add the remove access action button from the mockup.

```html
   <mat-action-row>
      <button mat-button color="accent">Remove access</button>
    </mat-action-row>
```

And tweaked the Material theme to get the accent color to be roughly close to that in the mockup

Finally, adopt the right header color by forking the mat-red palette and modifying the middle (500) color to use the correct color. This is a quick and dirty hack
that gets things mostly looking the right color without yet doing the full work of building a Material palette.
