# Filling out the Data & personalization page

In the mockup, the "Data & personalization" page also shows roles and preferences.

Okay. Let's add those. I'm re-tracing the steps for modeling the Application and Application Details components and defining the data model for application.

`ng generate component campus-role`

and

`ng generate component campus-role-detail`

From the mockup, looks like roles have a name and an activity status. They're in an expansion panel so I imagine there's more detail about them to look at, maybe a description.

So I model that.

```
export interface CampusRole {
  name: string;
  description: string;
  activeness: boolean;
}
```

Status is probably more complicated than just active or inactive, but modeling it as a boolean for now is simple and is an opportunity to try out a toggle in the UI.

Build out the card showing the roles

```html
<mat-card>
  <mat-card-title>Campus roles</mat-card-title>
  <mat-card-content>
  <p>Manage your relationships with the university.</p>

  <!-- <mat-accordian multi> -->
    <mat-expansion-panel class="expansion-panel-headers-align" *ngFor="let campusRole of campusRoles">
      <mat-expansion-panel-header>
        <mat-panel-title>{{campusRole.name}}</mat-panel-title>
        <mat-panel-description><span *ngIf="campusRole.activeness">Active</span><span *ngIf="!campusRole.activeness">Not active</span></mat-panel-description>
      </mat-expansion-panel-header>
      <app-campus-role-detail [campusRole]="campusRole"></app-campus-role-detail>
      <mat-action-row>
        <mat-slide-toggle [(ngModel)]="campusRole.activeness">Active</mat-slide-toggle>
      </mat-action-row>
    </mat-expansion-panel>
  <!-- </mat-accordian> -->

  </mat-card-content>
  </mat-card>
```

and the details pane for when role panels expand

```html
div class="campus-role-detail" *ngIf="campusRole">

  <p>{{campusRole.description}}</p>

</div>
```

Note the Material slide-toggle. De-activating your university relationship with the click of a toggle seems a bit drastic, but it at least makes for a good demo.

Also, now that there's enough content to scroll the page, the fixed positioning of the sidenav feels goofy, so make it too scroll away as if attached to the toolbar.

Could do the same sort of thing for the "General preferences" on that page as well. That'd be more of the same, so moving on instead.
