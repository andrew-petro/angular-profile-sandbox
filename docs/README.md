# In the docs directory

Hint: the changes in the commit history roughly match these steps, so you can there see all the details of the code beyond the broad strokes in the narration.

+ [01 Starting from nothing](./01-begin.md)
+ [02 Model something](./02-model-something.md)
+ [03 Application details](./03-application-details.md)
+ [04 Material stubs](./04-material-stubs.md)
+ [05 Roles and preferences](./05-roles-and-prefs.md)
+ [06 Navigation](./06-navigation.md)
